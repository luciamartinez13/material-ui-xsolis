module.exports = {
  settings: {
    react: {
      version: '16.0'
    }
  },
  env: {
    browser: true,
    'jest/globals': true
  },
  extends: [
    'plugin:react/recommended',
    'standard',
    'eslint:recommended',
    'plugin:@typescript-eslint/recommended',
    'prettier'
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 2020,
    sourceType: 'module'
  },
  plugins: ['react', '@typescript-eslint', 'jest'],
  rules: {
    'react/prop-types': 'off',
    'jest/no-disabled-tests': 'warn',
    'jest/no-focused-tests': 'error',
    'jest/no-identical-title': 'error',
    'jest/prefer-to-have-length': 'warn',
    'jest/valid-expect': 'error',
    'no-use-before-define': 'off',
    '@typescript-eslint/no-use-before-define': ['error'],
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    'no-tabs': 'off',
    'max-lines': [
      'error',
      { max: 200, skipBlankLines: true, skipComments: true }
    ],
    'max-depth': ['error', 2],
    'max-params': ['error', 3],
    'prefer-const': 'error',
    'react/react-in-jsx-scope': 'off'
  }
}
