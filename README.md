# XSOLIS React Template

## Linting
- ESLint using StandardJS rules.

## Conventions

- JSX Components are to be named using PascalCase.
- Utility functions, handler functions, variables and objects should use camelCase or SCREAMING_SNAKE_CASE for constant variables.
- A file in this project should not exceed 200 lines. Consider breaking down components and functions and unit testing them separately before using.
- Every file created should have a descriptive name. Only use index.* type of files for modules that export multiple files.
- Prefer using Functional components and custom react hooks to encapsulate stateful logic. 

## Project Structure
<details class="detailed-explanation" open="">

-   `/src`
    -   `index.tsx`: Entry point file that renders the React component tree
    -   `/app`
        -   `store.ts`: store setup
        -   `App.tsx`: root React component
    -   `/common`: hooks, generic components, utils, etc
		-   `/services`: httpClient instance and their services.
    -   `/features`: contains all "feature folders"
        -   `/Features.tsx`: contains all the feature modules imported as well as the react-router logic where routes are mapped to their components.
        -   `/rootSaga.ts`: contains all the imported sagas from the feature modules.
        -   `/rootReducer.ts`: contains all the imported reducers from the feature slices.
        -   `/Counter`: a single feature folder
            -   `counter.saga.ts`: Redux Saga logic associated with side-effects, any async calls are to be handled by sagas.
            -   `counter.saga.test.ts`: Redux Saga tests
            -   `counter.slice.ts`: Redux reducer logic and associated actions.
            -   `counter.slice.test.ts`: Redux reducer tests.
            -   `Counter.tsx`: a React feature component.

`/app` contains app-wide setup that depends on all the other folders.

`/common` contains truly generic and reusable utilities and components.

`/features` has folders that contain all functionality related to a specific feature. In this example, `counter.slice.ts` is a "duck"-style file that contains a call to RTK's `createSlice()` function, and exports the slice reducer and action creators.

</details>

## Asynchronous Strategy

-   We use Redux Saga to breakout async logic from reducers and be able to test them in isolation
-   Please refer to the documentation for any examples
-   https://redux-saga.js.org/docs/introduction/GettingStarted

## Configuration

-   Any environment or configuration based variables should be loaded on src/configuration.ts and exported so all other modules can consume from one source file.
