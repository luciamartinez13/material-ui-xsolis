import React from 'react'
import NavBar from './components/NavBar'
import GridContent from './components/GridContent'

import { makeStyles, createStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(() =>
  createStyles({
    mainDiv: { backgroundColor: '#f3f8fc', height: '100vh' }
  })
)

function App() {
  const classes = useStyles()
  return (
    <div className={classes.mainDiv}>
      <NavBar />
      <GridContent />
    </div>
  )
}

export default App
