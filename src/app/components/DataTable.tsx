import * as React from 'react'
import { makeStyles, createStyles } from '@material-ui/core/styles'
import { DataGrid } from '@material-ui/data-grid'
import { useDemoData } from '@material-ui/x-grid-data-generator'

const useStyles = makeStyles(() =>
  createStyles({
    rootTable: {
      flexGrow: 1,
      height: '70vh',
      width: '100%'
    }
  })
)

export default function DataTable() {
  const [pageSize, setPageSize] = React.useState<number>(10)

  const { data } = useDemoData({
    dataSet: 'Commodity',
    rowLength: 100,
    maxColumns: 6
  })

  const classes = useStyles()

  return (
    <div className={classes.rootTable}>
      <DataGrid
        filterModel={{
          items: [
            {
              columnField: 'commodity',
              operatorValue: 'contains',
              value: ''
            }
          ]
        }}
        pageSize={pageSize}
        onPageSizeChange={(newPageSize) => setPageSize(newPageSize)}
        rowsPerPageOptions={[5, 10, 20]}
        pagination
        {...data}
      />
    </div>
  )
}
