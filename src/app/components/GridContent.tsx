import * as React from 'react'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import GridSearch from './GridSearch'

import DataTable from './DataTable'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      justifyContent: 'center'
    },
    paper: {
      padding: '20px',
      textAlign: 'center',
      color: theme.palette.text.secondary
    }
  })
)

export default function GridContent() {
  const classes = useStyles()
  return (
    <Grid container className={classes.root} spacing={0}>
      <Grid item xs={8}>
        <Paper className={classes.paper} elevation={0}>
          <GridSearch />
        </Paper>
      </Grid>
      <Grid item xs={8}>
        <Paper className={classes.paper} elevation={0}>
          <DataTable />
        </Paper>
      </Grid>
    </Grid>
  )
}
