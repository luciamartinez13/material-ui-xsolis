import * as React from 'react'
import { XButton } from 'xsolis-component-library'
import SearchIcon from '@material-ui/icons/Search'
import InputBase from '@material-ui/core/InputBase'
import Typography from '@material-ui/core/Typography'
import Grid, { GridSpacing } from '@material-ui/core/Grid'
import FilterListIcon from '@material-ui/icons/FilterList'
import { createStyles, fade, Theme, makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      justifyContent: 'center'
    },
    paper: {
      padding: '20px',
      textAlign: 'center',
      color: theme.palette.text.secondary
    },

    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.black, 0.04),
      '&:hover': {
        backgroundColor: fade(theme.palette.common.black, 0.25)
      },
      marginLeft: 0,
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(1),
        width: 'auto'
      }
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    },
    inputRoot: {
      color: 'inherit'
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        width: '12ch',
        '&:focus': {
          width: '20ch'
        }
      }
    }
  })
)

export default function GridSearch() {
  const classes = useStyles()
  return (
    <Grid container spacing={2}>
      <Grid item xs={4} lg={3}>
        <Typography variant='h5'>Client Facility</Typography>
      </Grid>
      <Grid item xs={4} lg={3}>
        <div className={classes.search}>
          <div className={classes.searchIcon}>
            <SearchIcon />
          </div>
          <InputBase
            placeholder='Search…'
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput
            }}
            inputProps={{ 'aria-label': 'search' }}
          />
        </div>
      </Grid>
      <Grid item xs={4} lg={3}>
        <Grid container justify='center'>
          <Grid item>
            <FilterListIcon />
          </Grid>
          <Grid item>
            <Typography variant='h6'> Filter</Typography>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12} lg={3}>
        <XButton $backgroundColor='#0064c8' size='medium'>
          Add New
        </XButton>
      </Grid>
    </Grid>
  )
}
