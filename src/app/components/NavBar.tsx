import React from 'react'
import { XNav, XBox } from 'xsolis-component-library'

export default function NavBar() {
  return (
    <div>
      <XNav $backgroundColor='#0d2457'>
        <XBox>
          <img
            style={{ height: '2.5rem', padding: '1.7rem 1.5rem 1rem 1rem' }}
            src='./image.png'
          />
        </XBox>
      </XNav>
    </div>
  )
}
