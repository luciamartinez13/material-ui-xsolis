import React from 'react'
import styled, { keyframes } from 'styled-components'
import { cssTransition, ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

const simpleFade = cssTransition({
  enter: 'fade-in',
  exit: 'fade-out',
  collapse: false
})

const fadeIn = keyframes`
     0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  `
const fadeOut = keyframes`
    0% {
      opacity: 1;
    }
    100% {
      opacity: 0;
    }
  `
const ToastStyled = styled(ToastContainer)`
  .fade-in {
    animation: 0.5s ${fadeIn};
  }
  .fade-out {
    animation: 0.5s ${fadeOut};
  }
`
export function AlertContainer () {
  return (
    <>
      <ToastStyled
        transition={simpleFade}
        position='top-center'
        limit={1}
        autoClose={4000}
        hideProgressBar
      />
    </>
  )
}
