import { useMemo } from 'react'

/**
 * Custom hook to create a memoized object of handler functions
 */
export function useHandlers<T = unknown> (
  configureHandlers: T,
  dependencies: React.DependencyList
) {
  return useMemo<T>(() => configureHandlers, dependencies)
}
