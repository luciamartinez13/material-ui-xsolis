import { useState, useMemo, BaseSyntheticEvent } from 'react'

/**
 * Custom hook to encapsulate input element's stateful logic
 */
export function useInput (initialValue = '') {
  const [state, setState] = useState(initialValue)

  const handlers = useMemo(
    () => ({
      handleInputChange: (e: BaseSyntheticEvent) => {
        setState(e.target.value)
      },
      resetInput: () => {
        setState('')
      }
    }),
    []
  )

  return [state, handlers]
}
