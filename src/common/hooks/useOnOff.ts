import { useState, useMemo } from 'react'

/**
 * Custom hook to setup a on and off switch type of stateful action
 */
export function useOnOff () {
  const [state, setState] = useState('off')

  const handlers = useMemo(
    () => ({
      on: () => {
        setState('on')
      },
      off: () => {
        setState('off')
      },
      toggle: () => {
        setState((s) => (s === 'on' ? 'off' : 'on'))
      }
    }),
    []
  )

  return [state, handlers]
}
