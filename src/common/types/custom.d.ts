import React from 'react'

/**
 * Modify one property of an interface or type
 */
export type Modify<T, R> = Omit<T, keyof R> & R

/**
 * React Form SyntheticEvent but allows to add e.target.name types to the target property of an event handler
 */
export type CustomEventTarget<T> = React.SyntheticEvent['target'] & T
