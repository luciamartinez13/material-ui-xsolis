import { toast } from 'react-toastify'

/**
 * Inversion of control module for alerting users in the UI. Cleans up alerts first before sending new ones. toastId is needed here in order to only display one toast at a time.
 */
export function alertOn () {
  return {
    success (msg = 'Success', autoCloseDelay = 5000) {
      toast.success(msg, { toastId: 1, autoClose: autoCloseDelay })
    },
    warning (msg = 'Warning', autoCloseDelay = 5000) {
      toast.warn(msg, { toastId: 2, autoClose: autoCloseDelay })
    },
    error (msg = 'Error', autoCloseDelay = 5000) {
      toast.error(msg, { toastId: 3, autoClose: autoCloseDelay })
    },
    info (msg = 'Info', autoCloseDelay = 5000) {
      toast.info(msg, { toastId: 4, autoClose: autoCloseDelay })
    },
    dismiss (id: any = undefined) {
      id ? toast.dismiss(id) : toast.dismiss()
    }
  }
}
