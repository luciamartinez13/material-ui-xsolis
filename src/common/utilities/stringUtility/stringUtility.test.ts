import { stringUtility } from './stringUtility'

describe('stringUtility', () => {
  const testStrings = [
    'lives-Down_by-the.River',
    'Lives`Down,by~the RIVER',
    'LivesDownBY  the RIVER'
  ]

  describe('toCamelCase', () => {
    it('returns camelCase string', () => {
      const expectedResult = 'livesDownByTheRiver'

      testStrings.forEach((str) => {
        expect(stringUtility.toCamelCase(str)).toEqual(expectedResult)
      })
    })
  })

  describe('toTitleCase', () => {
    it('returns Title Case string', () => {
      const expectedResult = 'Lives Down By The River'

      testStrings.forEach((str) => {
        expect(stringUtility.toTitleCase(str)).toEqual(expectedResult)
      })
    })
  })

  describe('toPascalCase', () => {
    it('returns PascalCase string', () => {
      const expectedResult = 'LivesDownByTheRiver'

      testStrings.forEach((str) => {
        expect(stringUtility.toPascalCase(str)).toEqual(expectedResult)
      })
    })
  })

  describe('toConstantCase', () => {
    it('returns CONSTANT_CASE string', () => {
      const expectedResult = 'LIVES_DOWN_BY_THE_RIVER'

      testStrings.forEach((str) => {
        expect(stringUtility.toConstantCase(str)).toEqual(expectedResult)
      })
    })
  })

  describe('toDotCase', () => {
    it('returns dot.case string', () => {
      const expectedResult = 'lives.down.by.the.river'

      testStrings.forEach((str) => {
        expect(stringUtility.toDotCase(str)).toEqual(expectedResult)
      })
    })
  })

  describe('toKebabCase', () => {
    it('returns kebab-case string', () => {
      const expectedResult = 'lives-down-by-the-river'

      testStrings.forEach((str) => {
        expect(stringUtility.toKebabCase(str)).toEqual(expectedResult)
      })
    })
  })

  describe('toLowerCase', () => {
    it('returns lowercase string', () => {
      const expectedResult = 'livesdownbytheriver'

      testStrings.forEach((str) => {
        expect(stringUtility.toLowerCase(str)).toEqual(expectedResult)
      })
    })
  })

  describe('toPathCase', () => {
    it('returns path/case string', () => {
      const expectedResult = 'lives/down/by/the/river'

      testStrings.forEach((str) => {
        expect(stringUtility.toPathCase(str)).toEqual(expectedResult)
      })
    })
  })

  describe('toSnakeCase', () => {
    it('returns snake_case string', () => {
      const expectedResult = 'lives_down_by_the_river'

      testStrings.forEach((str) => {
        expect(stringUtility.toSnakeCase(str)).toEqual(expectedResult)
      })
    })
  })

  describe('toSentenceCase', () => {
    it('returns Sentence case string', () => {
      const expectedResult = 'Lives down by the river'

      testStrings.forEach((str) => {
        expect(stringUtility.toSentenceCase(str)).toEqual(expectedResult)
      })
    })
  })
})
