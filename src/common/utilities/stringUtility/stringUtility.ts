import {
  camelCase,
  kebabCase,
  lowerCase,
  snakeCase,
  startCase,
  upperCase,
  upperFirst
} from 'lodash'

interface StringUtility {
  [method: string]: (str: string) => string
}

export const stringUtility: StringUtility = {
  toCamelCase (str) {
    return camelCase(str)
  },

  toTitleCase (str) {
    return startCase(camelCase(str))
  },

  toPascalCase (str) {
    return startCase(camelCase(str)).replace(/ /g, '')
  },

  toConstantCase (str) {
    return upperCase(str).replace(/ /g, '_')
  },

  toDotCase (str) {
    return lowerCase(str).replace(/ /g, '.')
  },

  toKebabCase (str) {
    return kebabCase(str)
  },

  toLowerCase (str) {
    return lowerCase(str).replace(/ /g, '')
  },

  toPathCase (str) {
    return lowerCase(str).replace(/ /g, '/')
  },

  toSnakeCase (str) {
    return snakeCase(str)
  },

  toSentenceCase (str) {
    return upperFirst(lowerCase(str))
  }
}
