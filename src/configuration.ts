/**
 * Environment variables should be loaded here
 */
const configuration = {
  environment: process.env.REACT_APP_STAGE || 'dev'
}
export default configuration
