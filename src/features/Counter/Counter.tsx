import React from 'react'
import { XWrapper } from 'xsolis-component-library'
import { useHandlers } from '../../common/hooks/useHandlers'
import { useCounterDispatch, useCounterSelector } from './counter.slice'

function Counter () {
  const { loading, value } = useCounterSelector()
  const { dispatch, actions } = useCounterDispatch()
  const [amountToIncrement, setAmountToIncrement] = React.useState(1)

  const handlers = useHandlers(
    {
      changeAmountToIncrement: (e: React.BaseSyntheticEvent) =>
        setAmountToIncrement(Number(e.target.value)),
      increment: () => dispatch(actions.increment(amountToIncrement)),
      reset: () => dispatch(actions.reset()),
      incrementAsync: () =>
        dispatch({
          type: 'counter/incrementAsync',
          payload: amountToIncrement
        })
    },
    [dispatch, setAmountToIncrement, amountToIncrement]
  )

  return (
    <XWrapper>
      <div>
        <h1>Counter using Redux Saga and Redux Toolkit</h1>
        <h2>Current Count: {loading ? 'loading...' : value}</h2>
        <label htmlFor='amountToIncrement'>Amount to Increment:</label>
        <input
          id='amountToIncrement'
          type='number'
          name='amountToIncrement'
          value={amountToIncrement}
          onChange={handlers.changeAmountToIncrement}
        />
        <button onClick={handlers.increment}>
          Increment by:{amountToIncrement}
        </button>
        <button onClick={handlers.incrementAsync}>
          Increment Async using Saga
        </button>
        <button onClick={handlers.reset}>reset</button>
      </div>
    </XWrapper>
  )
}

export default Counter
