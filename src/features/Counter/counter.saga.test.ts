import { call, put } from 'redux-saga/effects'
import { incrementAsync, delay } from './counter.saga'
import { counterActions } from './counter.slice'

describe('incrementAsync Saga test', () => {
  it('incrementAsync Saga must call delay(1000)', () => {
    const gen = incrementAsync(counterActions.increment(1))
    expect(gen.next().value).toEqual(put({ type: 'counter/pending' }))
    expect(gen.next().value).toEqual(call(delay, 1000))
    expect(gen.next().value).toEqual(
      put({ type: 'counter/increment', payload: 1 })
    )
    expect(gen.next().value).toEqual(put({ type: 'counter/fulfilled' }))
  })
})
