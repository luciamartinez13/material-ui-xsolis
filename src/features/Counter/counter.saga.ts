import { call, put, takeLatest } from '@redux-saga/core/effects'
import { PayloadAction } from '@reduxjs/toolkit'
import { counterActions } from './counter.slice'

// Simulate an asynchronous call, it would be the same way for an http request.
export const delay = (ms: number) =>
  new Promise((resolve) => setTimeout(resolve, ms))

// Our worker Saga: will perform the async increment task
export function * incrementAsync (action: PayloadAction<number>) {
  yield put(counterActions.pending())
  yield call(delay, 1000)
  yield put(counterActions.increment(action.payload))
  yield put(counterActions.fulfilled())
}
// Our watcher Saga: will perform the worker saga upon an action is dispatched with that type INCREMENT_ASYNC
export function * watchIncrementAsync () {
  yield takeLatest('counter/incrementAsync', incrementAsync)
}
