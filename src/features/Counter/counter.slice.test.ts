import { counterReducer, counterActions, CounterState } from './counter.slice'

describe('counterSlice', () => {
  let state: CounterState
  it('should increment by 1', () => {
    state = {
      value: 0,
      loading: false
    }
    const actual = counterReducer(state, counterActions.increment(1))
    expect(actual).toEqual({ value: 1, loading: false })
  })
  it('counterActions.pending should set loading to true', () => {
    state = {
      value: 0,
      loading: false
    }
    const actual = counterReducer(state, counterActions.pending())
    expect(actual).toEqual({ value: 0, loading: true })
  })
  it('counterActions.fulfilled should set loading to false from true', () => {
    state = {
      value: 0,
      loading: true
    }
    const actual = counterReducer(state, counterActions.fulfilled())
    expect(actual).toEqual({ value: 0, loading: false })
  })
})
