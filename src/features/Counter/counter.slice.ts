import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { useAppDispatch, useAppSelector } from '../../common/hooks/reduxHooks'

export interface CounterState {
  value: number
  loading: boolean
}

export const initialState: CounterState = {
  value: 0,
  loading: false
}

const counterSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    reset () {
      return initialState
    },
    increment (state, action: PayloadAction<number>) {
      state.value += Number(action.payload)
    },
    pending (state) {
      state.loading = true
    },
    fulfilled (state) {
      state.loading = false
    }
  }
})

export const counterReducer = counterSlice.reducer
export const counterActions = counterSlice.actions

export function useCounterSelector () {
  return useAppSelector((state) => state.counter)
}
export function useCounterDispatch () {
  const dispatch = useAppDispatch()

  return { dispatch, actions: counterActions }
}
