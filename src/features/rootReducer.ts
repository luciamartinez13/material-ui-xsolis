import { combineReducers } from 'redux'
import { counterReducer } from './Counter/counter.slice'

export default combineReducers({
  counter: counterReducer
})
