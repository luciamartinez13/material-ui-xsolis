// src/features/rootSaga.ts - Import all sagas from feature modules
import { all } from '@redux-saga/core/effects'
import { watchIncrementAsync } from './Counter/counter.saga'

function * testSaga () {
  yield console.log('sagas running...')
}
// remember to call the imported sagas
export default function * rootSaga () {
  yield all([testSaga(), watchIncrementAsync()])
}
